import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

import { Configuration } from '../ConfigSystems/constants';
import { Router } from '@angular/router';


@Injectable()
export class Service {

  public lenguajeTable;
  public lengthMenu;
  public configDataTable;

  constructor(private http: Http,
  private _url: Configuration,
  private router: Router) {

  }

  

  public redirect() {
    this.router.navigate(['/401']);
  }

  public ConfigDataTable() {
    this.lenguajeTable = {
      "search": "Buscar:",
      "paginate": {
          "first": "Primero",
          "last": "Ultimo",
          "next": "Siguiente",
          "previous": "Anterior"
      },
      "emptyTable": "Datos no encontrados",
      "info": "Mostrando _START_ a  _END_ de _TOTAL_ Datos",
      "lengthMenu": "Cargados _MENU_ Datos",
      "zeroRecords": "No se encontrarón resultados",
      "infoEmpty": "Mostrando 0  de _MAX_ Datos",
      "infoFiltered": "(filtrado entre _MAX_ total de Datos)"
  
    };
  
    this.lengthMenu = [
      [10, 25, 50, -1],
      [10, 25, 50, "Todos"]
    ];
  
    this.configDataTable = {
      pagingType: 'full_numbers',
      language: this.lenguajeTable,
      lengthMenu: this.lengthMenu
    }
  
    return this.configDataTable;
  }

  public ConfigDataTableAllRegister() {
    this.lenguajeTable = {
      "search": "Buscar:",
      "paginate": {
          "first": "Primero",
          "last": "Ultimo",
          "next": "Siguiente",
          "previous": "Anterior"
      },
      "emptyTable": "Datos no encontrados",
      "info": "Mostrando _START_ a  _END_ de _TOTAL_ Datos",
      "lengthMenu": "Cargados _MENU_ Datos",
      "zeroRecords": "No se encontrarón resultados",
      "infoEmpty": "Mostrando 0  de _MAX_ Datos",
      "infoFiltered": "(filtrado entre _MAX_ total de Datos)"
  
    };
  
    this.lengthMenu = [
     // [1000, 2500, 5000, -1],
      //[1000, 2500, 5000, "Todos"]
      [-1],
      ['Todos']
    ];
  
    this.configDataTable = {
      pagingType: 'full_numbers',
      language: this.lenguajeTable,
      lengthMenu: this.lengthMenu
    }
  
    return this.configDataTable;
  }
  
  
}
