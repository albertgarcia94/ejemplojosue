import {
  NgModule
} from '@angular/core';
import {
  RouterModule,
  Routes
} from '@angular/router';

import {
  LayoutComponent
} from './/layouts/layout.component';

import {
  HomeComponent
} from './pages/home/home.component';

import {
  LoginComponent
} from './pages/Seguridad/login/login.component';

import {
  ForgotPasswordComponent
} from './pages/Seguridad/forgot-password/forgot-password.component'
import { Error400Component } from './pages/Seguridad/error400/error400.component';
import { RegisterComponent } from './pages/Seguridad/register/register.component';
// import { RegisterComponent } from './pages/Seguridad/register/register.component';

const routes: Routes = [{
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },

  {
    "path": "",
    "component": LayoutComponent,
    "children": [
      {
        path: 'mantenimientos',
        loadChildren: './pages/Mantenimientos/mantenimiento.module#MantenimientoModule'
      },
      {
        path: 'seguridad',
        loadChildren: './pages/Seguridad/seguridad.module#SeguridadModule'
      },
      {
        path: 'usuario',
        loadChildren:'./pages/Usuario/usuario-module#UsuarioModule'
      },
      {
        path: "index",
        component: HomeComponent
      },
    ]
  },
  {
    "path": "login",
    "component": LoginComponent
  },
  {
    "path": "recuperar_password",
    "component": ForgotPasswordComponent
  },{
    "path": "registro",
    "component": RegisterComponent
  },
  {
    "path": "**",
    "component": Error400Component
  },

];

@NgModule({
  declarations: [
    HomeComponent,
    ForgotPasswordComponent,
    LoginComponent,
    Error400Component,
    // RegisterComponent,

  ],
  imports: [RouterModule.forRoot(routes)],
  exports: [
    RouterModule,
  ]
})

export class AppRoutingModule {}
