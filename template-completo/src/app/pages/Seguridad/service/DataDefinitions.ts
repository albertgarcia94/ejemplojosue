import {
    Injectable
  } from '@angular/core';
  
  
  import {
    FormGroup,
    Validator,
    FormControl,
    Validators
  } from '@angular/forms';
  
  @Injectable()
  export class DataDefinitions {
    //estados generales
   //1 del catalogo para traer los estados
    public uriEstados="detallecatalogo/1"
    public estados = []
    //1 cuando es nuevo, 2 para editar
    public action = 1

    
    public form: FormGroup;

    //Roles
    rol:any={};
    dataRoles=[];
    uriRoles="";

  //register Hijo
  public uriRegister ="register"
  public registerArray =[];
  public register;
  
  }
  