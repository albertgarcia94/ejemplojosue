import {
  BsModalService,
  BsModalRef
} from 'ngx-bootstrap/modal';

import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  TemplateRef
} from '@angular/core';
import {
  DataTableDirective
} from 'angular-datatables';
import {
  Subject
} from 'rxjs';
import {
  ToastrService
} from 'ngx-toastr';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';

import {
  Service
} from '../../../service/service.service'
import {
  ServiceConfig
} from '../../../ServiceGlobal/ConfigDataTable';
import {
  SwallAlertGlobal
} from '../../../ServiceGlobal/Alertas'
import {
  Validaciones
} from '../../../ServiceGlobal/Validaciones'

import {DataDefinitions} from '../service/DataDefinitions';
import {ServiceSeguridad} from '../service/ServiceSeguridad'

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css'],
  providers: [ServiceConfig, SwallAlertGlobal, Validaciones, Service,DataDefinitions,ServiceSeguridad]
})
export class RolesComponent implements AfterViewInit, OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger = new Subject();
  modalRef: BsModalRef;

  titulo;

  constructor(
    private modalService: BsModalService,
    private configDatatable: ServiceConfig,
    private toastr: ToastrService,
    private swall: SwallAlertGlobal,
    private validar: Validaciones,
    private service: Service,
    private serviceSegurida: ServiceSeguridad,
    private serviceDefinitions: DataDefinitions
  ) {
    this.validar.validationCharacter();
    this.validar.replaceSpace();

    this.dtOptions = this.configDatatable.ConfigDataTable();

    // se llama en el constructor para que solo cargue una vez
    this.loadEstados()

  }


  ngOnInit() {

    this.titulo="Agregar nuevo ayuda"

    this.serviceDefinitions.form = new FormGroup({
      'rol': new FormControl(null, [
        Validators.required,
      ]),
      'descripcion': new FormControl(null, [
        Validators.required,
      ]),
      'abreviatura': new FormControl(null, [
        Validators.required,
      ]),
      'estadoRegistro': new FormControl(null, [
        Validators.required
      ]),
    })

    this.serviceDefinitions.rol = {
      idServiciosRol: "",
      rol: "",
      descripcion: "",
      abreviatura: "",
      estadoRegistro:""
    }

    this.loadRoles()

  }


  loadEstados() {


    // se guarda en sesion storage para que solo cargue una vez en todo el sistema, esto
    //se agrega en todos los componentes
    if(sessionStorage.getItem('estados')){
        this.serviceDefinitions.estados=JSON.parse(sessionStorage.getItem('estados'))
        return
  
      }
      
    this.serviceSegurida.Get(this.serviceDefinitions.uriEstados)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.serviceDefinitions.dataRoles = data.data;
            sessionStorage.setItem("estados",JSON.stringify(data.data))
          } else if (data.status == 400) {
            this.toastr.warning(data.message, 'Advertencia!');

          } else {
            this.toastr.error(data.message, 'Error!');

          }
          this.rerender();
        },
        Error => {
          if (Error.status == 401) {
            this.service.redirect();
          } else {
            this.toastr.error('Ocurrio un error con el servicio', 'Error!');
          }
        }
      );

  }

  loadRoles() {


    // se guarda en sesion storage para que solo cargue una vez en todo el sistema, esto
    //se agrega en todos los componentes
    if(sessionStorage.getItem('tipoAyuda')){
        this.serviceDefinitions.estados=JSON.parse(sessionStorage.getItem('estados'))
        return
  
      }
      
    this.serviceSegurida.Get(this.serviceDefinitions.uriRoles)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.serviceDefinitions.dataRoles = data.data;
            sessionStorage.setItem("tipoAyuda",JSON.stringify(data.data))
          } else if (data.status == 400) {
            this.toastr.warning(data.message, 'Advertencia!');

          } else {
            this.toastr.error(data.message, 'Error!');

          }
          this.rerender();
        },
        Error => {
          if (Error.status == 401) {
            this.service.redirect();
          } else {
            this.toastr.error('Ocurrio un error con el servicio', 'Error!');
          }
        }
      );

  }

  Save(item) {
    var body;
    if (this.serviceDefinitions.action == 1) {
      body = JSON.stringify({
     
        rol: item.rol,
        descripcion: item.descripcion,
        abreviatura:item.abreviatura,
        estadoRegistro:item.estadoRegistro
      })
    } else {
      body = JSON.stringify({
        idServiciosRol: item.idServiciosRol,
        rol: item.rol,
        descripcion: item.descripcion,
        abreviatura:item.abreviatura,
        estadoRegistro:item.estadoRegistro
      });
    }


     this.serviceSegurida.Send(body, this.serviceDefinitions.uriRoles, this.serviceDefinitions.action)
        .subscribe(
          data => {
            if (data.status == 200) {
              this.swall.swallAlert(data.message, 1)
              this.hide()
            } else if (data.status == 402) {
              this.swall.swallAlert(data.message, 2)

            }else {
              this.swall.swallAlert(data.message, 3)
            }
          },
          Error => {
            if (Error.status == 401) {
              this.service.redirect();
            } else {
              this.swall.swallAlert("Ocurrio un error!", 3)
            }
          }
        );
  }

  edit(itemplate: TemplateRef < any > , item) {
    if (item != 0 && item != null && item != "") {
      this.serviceDefinitions.rol = item;
      this.titulo = "Editar ayuda ";
      this.serviceDefinitions.action = 2;

    } else {
      this.titulo = "Nuevo ayuda";
      this.serviceDefinitions.action = 1;
    }
    this.modalRef = this.modalService.show(itemplate,this.configDatatable.config);

  }

  show(template: TemplateRef < any > ): void {
    this.modalRef = this.modalService.show(template,this.configDatatable.config);
  }

  hide(): void {
    this.modalRef.hide()
    this.serviceDefinitions.action = 1;
    this.ngOnInit()
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }


  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }
  Back() {
    window.history.back();
  }

  Next() {
    window.history.forward();
  }
}