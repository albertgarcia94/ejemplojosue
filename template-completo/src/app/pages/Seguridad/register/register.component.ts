import {
  BsModalService,
  BsModalRef
} from 'ngx-bootstrap/modal';

import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  TemplateRef
} from '@angular/core';
import {
  DataTableDirective
} from 'angular-datatables';
import {
  Subject
} from 'rxjs';
import {
  ToastrService
} from 'ngx-toastr';
import {
  FormGroup,
  FormControl,
  Validators,
  
} from '@angular/forms';
import { Service } from '../../../service/service.service';
import { SwallAlertGlobal } from '../../../ServiceGlobal/Alertas';
import { DataDefinitions } from '../service/DataDefinitions';
import { ServiceSeguridad } from '../service/ServiceSeguridad';
import {
  Validaciones
} from '../../../ServiceGlobal/Validaciones'
import {
  ServiceConfig
} from '../../../ServiceGlobal/ConfigDataTable';
declare var $:any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers:  [ServiceConfig,SwallAlertGlobal, Service, ServiceSeguridad, DataDefinitions, Validaciones]
})
export class RegisterComponent implements OnInit, AfterViewInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger = new Subject();
  modalRef: BsModalRef;


  constructor(
   
   
    private swall: SwallAlertGlobal,
    private service: Service,
    private serviceSeguridad : ServiceSeguridad,
    
    private serviceDefinitions: DataDefinitions
    
  ) {
    
   
   }

  ngOnInit() {

    this.serviceDefinitions.form = new FormGroup({
      'nombre': new FormControl(null, [
        Validators.required,
        // Validators.minLength(3)
      ]),
      'identificacion': new FormControl(null, [
        Validators.required,
      ]),
      'telefono': new FormControl(null, [
        Validators.required,
      ]),
      'correo': new FormControl(null, [
        Validators.required,
      ]),
      'usuario': new FormControl(null, [
        Validators.required
      ]),
      'genero': new FormControl(null, [
        Validators.required
      ])

    })
    ;
    this.serviceDefinitions.register = {
      id:"",
      nombre: "",
      identificacion: "",
      telefono: "",
      correo:"",
      usuario:"",
      genero:"",
      contrasenia:""
    };
    
  }

  ngAfterViewInit() {
    
    
  }
  


  hide(): void {
    this.modalRef.hide()
    this.serviceDefinitions.action = 1;
    this.ngOnInit()
  }

  Save(item) {
    var body;
    if (this.serviceDefinitions.action == 1) {
      body = JSON.stringify({
      
        nombre:item.nombre,
        telefono:item.telefono,
        correo:item.correo,
        usuario:item.usuario,
        identificacion:item.identificacion,
        genero:item.genero,
        contrasenia:item.contrasenia
        
      
      })
    } else {
      body = JSON.stringify({
        
        
        id: 1,
        nombre:item.nombre,
          telefono:item.telefono,
          correo:item.correo,
          usuario:item.usuario,
          identificacion:item.identificacion,
          genero:item.genero,
          contrasenia:item.contrasenia
        
      });
    }

    

    this.serviceSeguridad.Send(body, this.serviceDefinitions.uriRegister, this.serviceDefinitions.action)
        .subscribe(
          data => {
            if (data.status == 200) {
              this.swall.swallAlert(data.message, 1)
              this.hide()
            } else if (data.status == 402) {
              this.swall.swallAlert(data.message, 2)

            }else {
              this.swall.swallAlert(data.message, 3)
            }
          },
          Error => {
            if (Error.status == 401) {
              this.service.redirect();
            } else {
              this.swall.swallAlert("Ocurrio un error!", 3)
            }
          }
        );
    }
}


