import {
    NgModule
  } from '@angular/core';
  import {
    Routes,
    RouterModule
  } from '@angular/router';
import { RegisterComponent } from './register/register.component';
  import {RolesComponent} from './roles/roles.component';
  import {SubMenuSeguridadComponent} from './sub-menu-seguridad/sub-menu-seguridad.component'
    const routes: Routes = [{
    path: '',
    data: {
      title: 'seguridad'
    },
    children: [

    {
      path: 'roles',
      component: RolesComponent,
      data: {
        title: ''
      }
    },
    {
      path: 'menu',
      component: SubMenuSeguridadComponent,
      data: {
        title: ''
      }
    },{
      path: 'registro',
      component: RegisterComponent,
      data: {
        title: ''
      }
    },
   ]
  }];
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class SeguridadRoutingModule {}
  