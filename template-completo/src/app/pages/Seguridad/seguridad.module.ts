import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import{SeguridadRoutingModule} from './seguridad-routing.module';
import { Error400Component } from './error400/error400.component';
import { RolesComponent } from './roles/roles.component';
import { SubMenuSeguridadComponent } from './sub-menu-seguridad/sub-menu-seguridad.component'
import { RegisterComponent } from './register/register.component';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    SeguridadRoutingModule,
    ModalModule.forRoot(),
    CommonModule,
    DataTablesModule
  ],
  declarations: [
  // Error400Component,
  RolesComponent,
  RegisterComponent,
  SubMenuSeguridadComponent,],
  exports: [
    ModalModule
  ]
})
export class SeguridadModule { }
