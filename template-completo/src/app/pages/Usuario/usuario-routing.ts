import {
    NgModule
  } from '@angular/core';
  import {
    Routes,
    RouterModule
  } from '@angular/router';
  import {
    DepartamentosComponent
  } from '../Mantenimientos/Departamentos/departamentos.components';
import { PaginaPrincipalComponent } from './pagina-principal/pagina-principal.component';
import { PerfilComponent } from './perfil/perfil.component';
import { PerfilesComponent } from './perfiles/perfiles.component';
import { PuestosUsuariosComponent } from './puestos-usuarios/puestos-usuarios.component';
import { RegisterComponent } from './register/register.component';
import { TareasAsignarComponent } from './tareas-asignar/tareas-asignar.component';
import { TareasRealizarComponent } from './tareas-realizar/tareas-realizar.component';
  
  const routes: Routes = [{
    path: '',
    data: {
      title: 'Usuario'
    },
    children: [{
      path: 'perfiles',
      component: PerfilesComponent,
      data: {
        title: ''
      }  
    },{
      path: 'puestos',
      component: PuestosUsuariosComponent,
      data: {
        title: ''
      }  
    },{
      path: 'tareas',
      component: TareasAsignarComponent,
      data: {
        title: ''
      }  
    },{
      path: 'perfil',
      component: PerfilComponent,
      data: {
        title: ''
      }  
    },{
      path: 'realizar',
      component: TareasRealizarComponent,
      data: {
        title: ''
      }  
    },{
      path: 'menusuario',
      component: PaginaPrincipalComponent,
      data: {
        title: ''
      },
      
    }
    
  ]
  }];
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class UsuarioRoutingModule {}