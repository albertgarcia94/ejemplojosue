import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TareasAsignarComponent } from './tareas-asignar.component';

describe('TareasAsignarComponent', () => {
  let component: TareasAsignarComponent;
  let fixture: ComponentFixture<TareasAsignarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TareasAsignarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TareasAsignarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
