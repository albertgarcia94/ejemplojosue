import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TareasRealizarComponent } from './tareas-realizar.component';

describe('TareasRealizarComponent', () => {
  let component: TareasRealizarComponent;
  let fixture: ComponentFixture<TareasRealizarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TareasRealizarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TareasRealizarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
