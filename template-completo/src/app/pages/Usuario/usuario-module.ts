import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import {UsuarioRoutingModule} from './usuario-routing';
import { PerfilesComponent } from './perfiles/perfiles.component';
import { PaginaPrincipalComponent } from './pagina-principal/pagina-principal.component';
import { PuestosUsuariosComponent } from './puestos-usuarios/puestos-usuarios.component';
import { TareasAsignarComponent } from './tareas-asignar/tareas-asignar.component';
import { PerfilComponent } from './perfil/perfil.component';
// import { RegisterComponent } from './register/register.component';
import { TareasRealizarComponent } from './tareas-realizar/tareas-realizar.component';


@NgModule({
    imports: [
      FormsModule,
      ReactiveFormsModule,
      UsuarioRoutingModule,
      ModalModule.forRoot(),
      CommonModule,
      DataTablesModule
    ],
    declarations: [
     PerfilesComponent,
     PuestosUsuariosComponent,
     TareasAsignarComponent,
     TareasRealizarComponent,
     PerfilesComponent,
     PerfilComponent,
    //  RegisterComponent,
     PaginaPrincipalComponent
      

    ],
  
    exports: [
      ModalModule
    ]
  })
  export class UsuarioModule { }