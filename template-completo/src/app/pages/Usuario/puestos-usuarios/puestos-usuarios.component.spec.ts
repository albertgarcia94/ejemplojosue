import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PuestosUsuariosComponent } from './puestos-usuarios.component';

describe('PuestosUsuariosComponent', () => {
  let component: PuestosUsuariosComponent;
  let fixture: ComponentFixture<PuestosUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PuestosUsuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PuestosUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
