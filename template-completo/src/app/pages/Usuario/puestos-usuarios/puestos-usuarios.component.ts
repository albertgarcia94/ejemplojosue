
import {
  BsModalService,
  BsModalRef
} from 'ngx-bootstrap/modal';

import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  TemplateRef
} from '@angular/core';
import {
  DataTableDirective
} from 'angular-datatables';
import {
  Subject
} from 'rxjs';
import {
  ToastrService
} from 'ngx-toastr';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  ServiceDeptos
} from '../ServiceDeptos/ServiceDeptos'
import {
  Service
} from '../../../service/service.service'

import {
  ServiceConfig
} from '../../../ServiceGlobal/ConfigDataTable';
import {
  SwallAlertGlobal
} from '../../../ServiceGlobal/Alertas'
import {
  Validaciones
} from '../../../ServiceGlobal/Validaciones'

import {
  DataDefinitions
} from '../ServiceDeptos/DataDefinitions'



@Component({
  selector: 'app-puestos-usuarios',
  templateUrl: './puestos-usuarios.component.html',
  styleUrls: ['./puestos-usuarios.component.css'],
  providers: [ServiceConfig, SwallAlertGlobal, Validaciones, Service,ServiceDeptos, DataDefinitions]
})
export class PuestosUsuariosComponent implements AfterViewInit, OnInit {
  
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger = new Subject();
  modalRef: BsModalRef;

  titulo;
  constructor(
    private modalService: BsModalService,
    private configDatatable: ServiceConfig,
    private toastr: ToastrService,
    private swall: SwallAlertGlobal,
    private service: Service,
    private serviceDefinitions: DataDefinitions) { 
    
  }

  ngOnInit() {
  }
  Back() {
    window.history.back();
  }
  show(template: TemplateRef < any > ): void {
    this.modalRef = this.modalService.show(template,this.configDatatable.config);

  }
  hide(): void {
    this.modalRef.hide()
    this.serviceDefinitions.action = 1;
    this.ngOnInit()
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

}

