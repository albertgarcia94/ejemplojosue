import {
  BsModalService,
  BsModalRef
} from 'ngx-bootstrap/modal';

import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  TemplateRef
} from '@angular/core';
import {
  DataTableDirective
} from 'angular-datatables';
import {
  Subject
} from 'rxjs';
import {
  ToastrService
} from 'ngx-toastr';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  ServiceDeptos
} from '../ServiceDeptos/ServiceDeptos'
import {
  Service
} from '../../../service/service.service'

import {
  ServiceConfig
} from '../../../ServiceGlobal/ConfigDataTable';
import {
  SwallAlertGlobal
} from '../../../ServiceGlobal/Alertas'
import {
  Validaciones
} from '../../../ServiceGlobal/Validaciones'

import {
  DataDefinitions
} from '../ServiceDeptos/DataDefinitions'

@Component({
  selector: 'app-perfiles',
  templateUrl: './perfiles.component.html',
  styleUrls: ['./perfiles.component.css'],
  providers: [ServiceConfig, SwallAlertGlobal, Validaciones, Service,ServiceDeptos, DataDefinitions]

})
export class PerfilesComponent implements AfterViewInit, OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger = new Subject();
  modalRef: BsModalRef;

  titulo;
  constructor(
    private modalService: BsModalService,
    private configDatatable: ServiceConfig,
    private toastr: ToastrService,
    private swall: SwallAlertGlobal,
    private service: Service,
    private serviceDeptos : ServiceDeptos,
    
    private serviceDefinitions: DataDefinitions
    ) { 
      this.serviceDefinitions.genero = [
        {id:"m",
      nombre:"masculino"},
      {id:"2",
      nombre:"femenino"}
      ]
    }

  ngOnInit() {
    this.loadUsuario();
    this.serviceDefinitions.form = new FormGroup({
      'nombre': new FormControl(null, [
        Validators.required,
        // Validators.minLength(3)
      ]),
      'identificacion': new FormControl(null, [
        Validators.required,
      ]),
      'telefono': new FormControl(null, [
        Validators.required,
      ]),
      'correo': new FormControl(null, [
        Validators.required,
      ]),
      'usuario': new FormControl(null, [
        Validators.required
      ]),
      'genero': new FormControl(null, [
        Validators.required
      ])

    })
    ;
    this.serviceDefinitions.usuario = {
      id:"",
      nombre: "",
      identificacion: "",
      telefono: "",
      correo:"",
      usuario:"",
      genero:"" 
    };
  }
  Back() {
    window.history.back();
  }
  show(template: TemplateRef < any > ): void {
    this.modalRef = this.modalService.show(template,this.configDatatable.config);

  }
  hide(): void {
    this.modalRef.hide()
    this.serviceDefinitions.action = 1;
    this.ngOnInit()
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

//aqui se cargan apis 

loadUsuario() {
  this.serviceDeptos.Get(this.serviceDefinitions.uriUsuarios)
    .subscribe(
      data => {
        if (data.status == 200) {
          this.serviceDefinitions.usuariosArray = data.data;
        } else if (data.status == 400) {
          this.toastr.warning(data.message, 'Advertencia!');

        } else {
          this.toastr.error(data.message, 'Error!');

        }
        this.rerender();
      },
      Error => {
        if (Error.status == 401) {
          this.service.redirect();
        } else {
          this.toastr.error('Ocurrio un error con el servicio', 'Error!');
        }
      }
    );
    

   

  
}

//aqui cargan las funciones 
rerender(): void {
  this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
    dtInstance.destroy();
    this.dtTrigger.next();
  });
}
edit(itemplate: TemplateRef < any > , item) {
  if (item != 0 && item != null && item != "") {
    this.serviceDefinitions.usuario = item;
    this.titulo = "Editar puesto ";
    this.serviceDefinitions.action = 2;

  } else {
    this.titulo = "Nuevo puesto";
    this.serviceDefinitions.action = 1;
  }
  this.modalRef = this.modalService.show(itemplate,this.configDatatable.config);

}
    Save(item) {
      var body;
      if (this.serviceDefinitions.action == 1) {
        body = JSON.stringify({
        
          nombre:item.nombre,
          telefono:item.telefono,
          correo:item.correo,
          usuario:item.usuario,
          identificacion:item.identificacion,
          genero:item.genero
        
        })
      } else {
        body = JSON.stringify({
          
          
          id: 1,
          nombre:item.nombre,
            telefono:item.telefono,
            correo:item.correo,
            usuario:item.usuario,
            identificacion:item.identificacion,
            genero:item.genero
          
        });
      }

      this.serviceDeptos.Send(body, this.serviceDefinitions.uriUsuarios, this.serviceDefinitions.action)
          .subscribe(
            data => {
              if (data.status == 200) {
                this.swall.swallAlert(data.message, 1)
                this.hide()
              } else if (data.status == 402) {
                this.swall.swallAlert(data.message, 2)

              }else {
                this.swall.swallAlert(data.message, 3)
              }
            },
            Error => {
              if (Error.status == 401) {
                this.service.redirect();
              } else {
                this.swall.swallAlert("Ocurrio un error!", 3)
              }
            }
          );
      }
}
