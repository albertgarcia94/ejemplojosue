import {
  Injectable
} from '@angular/core';


import {
  FormGroup,
  Validator,
  FormControl,
  Validators
} from '@angular/forms';

@Injectable()
export class DataDefinitions {

  //estados generales
 //1 del catalogo para traer los estados
  public uriEstados="detallecatalogo/1"
  public estados = []
  //1 cuando es nuevo, 2 para editar
  public action = 1

  public dataDeptos = []
  public deptos: any = {}
  public uriDeptos = "departamentos"

  //catalogo detalle
  public  paramConfig;
  public config={
    id:"",
    nombre:""
  }
  public uriCatalogo="detallecatalogo/"
  public dataCatalogo=[]
  public catalogo:any={}

  public form: FormGroup;

//Puestos
public uriPuesto="puestos";
public puesto: any={};
public dataPuesto=[];

//Ayudas
public uriAyudas="ayudas";
public ayuda:any={};
public dataAyuda=[];

//tipo ayuda
public uriTipoAyuda="detallecatalogo/5"
public dataTipoAyuda=[]

//Jerarquia
public configJerarquia={
  id:"4"
}
dataJerarquia=[];

//usuario hijo
public uriUsuarios ="usuario"
public usuariosArray = [];
public formUsuario : FormGroup;
public usuario;
public genero = [];


//register Hijo






}


