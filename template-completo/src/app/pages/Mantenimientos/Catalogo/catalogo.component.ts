import {
  BsModalService,
  BsModalRef
} from 'ngx-bootstrap/modal';

import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  TemplateRef
} from '@angular/core';
import {
  DataTableDirective
} from 'angular-datatables';
import {
  Subject
} from 'rxjs';
import {
  ToastrService
} from 'ngx-toastr';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  ActivatedRoute
} from "@angular/router";

import {
  Service
} from '../../../service/service.service'
import {
  ServiceDeptos
} from '../ServiceDeptos/ServiceDeptos'
import {
  ServiceConfig
} from '../../../ServiceGlobal/ConfigDataTable';
import {
  SwallAlertGlobal
} from '../../../ServiceGlobal/Alertas'
import {
  Validaciones
} from '../../../ServiceGlobal/Validaciones'
import {
  DataDefinitions
} from '../ServiceDeptos/DataDefinitions'

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.css'],
  providers: [ServiceConfig, SwallAlertGlobal, Validaciones, Service, ServiceDeptos, DataDefinitions]

})
export class CatalogoComponent implements AfterViewInit, OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger = new Subject();
  modalRef: BsModalRef;

  titulo;

  constructor(
    private modalService: BsModalService,
    private configDatatable: ServiceConfig,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private swall: SwallAlertGlobal,
    private validar: Validaciones,
    private service: Service,
    private serviceDeptos: ServiceDeptos,
    private serviceDefinitions: DataDefinitions
  ) {
    this.validar.validationCharacter();
    this.validar.replaceSpace();

    this.dtOptions = this.configDatatable.ConfigDataTable();

    // se llama en el constructor para que solo cargue una vez
    this.loadEstados()

    this.serviceDefinitions.paramConfig = this.route.params.subscribe(params => {
      this.serviceDefinitions.config.id = params['id_catalogo'];
      this.serviceDefinitions.config.nombre = params['descripcion'];

      this.serviceDefinitions.config.nombre = this.serviceDefinitions.config.nombre.split('_').join(' ');

    });


  }


  ngOnInit() {


    this.titulo = "Nuevo catalogo"
    this.loadCatalogo();
    this.serviceDefinitions.form = new FormGroup({
      'nombre': new FormControl(null, [
        Validators.required,
        Validators.maxLength(100),
        Validators.minLength(1)
      ]),
      'descripcion': new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(100)
      ]),
      'abreviatura': new FormControl(null, [
        Validators.required,
        Validators.maxLength(4),
        Validators.minLength(3)
      ]),

      'estadoRegistro': new FormControl(null, [
        Validators.required
      ])
    })

    this.serviceDefinitions.catalogo = {
      idDetalleCatalogo: "",
      nombre: "",
      descripcion: "",
      estadoRegistro: "",
      abreviatura: ""
    }
    

  }





  loadCatalogo() {

    this.serviceDeptos.Get(this.serviceDefinitions.uriCatalogo + this.serviceDefinitions.config.id)
      .subscribe(
        data => {
            this.serviceDefinitions.dataCatalogo=[]
          if (data.status == 200) {
            this.serviceDefinitions.dataCatalogo = data.data;
          } else if (data.status == 400) {
            this.toastr.warning(data.message, 'Advertencia!');

          } else {
            this.toastr.error(data.message, 'Error!');

          }
          this.rerender();
        },
        Error => {
          if (Error.status == 401) {
            this.service.redirect();
          } else {
            this.toastr.error('Ocurrio un error con el servicio', 'Error!');
          }
        }
      );

  }


  loadEstados() {


    // se guarda en sesion storage para que solo cargue una vez en todo el sistema, esto
    //se agrega en todos los componentes
    if (sessionStorage.getItem('estados')) {
      this.serviceDefinitions.estados = JSON.parse(sessionStorage.getItem('estados'))
      return

    }

    this.serviceDeptos.Get(this.serviceDefinitions.uriEstados)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.serviceDefinitions.estados = data.data;
            sessionStorage.setItem("estados", JSON.stringify(data.data))
          } else if (data.status == 400) {
            this.toastr.warning(data.message, 'Advertencia!');

          } else {
            this.toastr.error(data.message, 'Error!');

          }
          this.rerender();
        },
        Error => {
          if (Error.status == 401) {
            this.service.redirect();
          } else {
            this.toastr.error('Ocurrio un error con el servicio', 'Error!');
          }
        }
      );

  }

  Save(item) {
    var body;
    if (this.serviceDefinitions.action == 1) {
      body = JSON.stringify({
        nombre: item.nombre,
        descripcion: item.descripcion,
        estadoRegistro: item.estadoRegistro,
        abreviatura: item.abreviatura,
        idCatalogo: this.serviceDefinitions.config.id
      })
    } else {
      body = JSON.stringify({
        idDetalleCatalogo: item.idDetalleCatalogo,
        nombre: item.nombre,
        descripcion: item.descripcion,
        estadoRegistro: item.estadoRegistro,
        abreviatura: item.abreviatura,
        idCatalogo: this.serviceDefinitions.config.id
      });
    }


    this.serviceDeptos.Send(body, this.serviceDefinitions.uriCatalogo, this.serviceDefinitions.action)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.swall.swallAlert(data.message, 1)
            this.hide()
          } else if (data.status == 400) {
            this.swall.swallAlert(data.message, 2)

          } else {
            this.swall.swallAlert(data.message, 3)
          }
        },
        Error => {
          if (Error.status == 401) {
            this.service.redirect();
          } else {
            this.swall.swallAlert("Ocurrio un error!", 3)
          }
        }
      );
  }

  edit(itemplate: TemplateRef < any > , item) {
    if (item != 0 && item != null && item != "") {
      this.serviceDefinitions.catalogo = item;
      this.titulo = "Editar catalogo ";
      this.serviceDefinitions.action = 2;

    } else {
      this.titulo = "Nuevo catalogo";
      this.serviceDefinitions.action = 1;
    }
    this.modalRef = this.modalService.show(itemplate,this.configDatatable.config);

  }

  show(template: TemplateRef < any > ): void {
    this.modalRef = this.modalService.show(template,this.configDatatable.config);
  }

  hide(): void {
    this.modalRef.hide()
    this.serviceDefinitions.action = 1;
    this.loadCatalogo();
    this.ngOnInit()
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }


  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }



  Back() {
    window.history.back();
  }

  Next() {
    window.history.forward();
  }







}
