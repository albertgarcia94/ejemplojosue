import {
  NgModule
} from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';
import {
  DepartamentosComponent
} from './Departamentos/departamentos.components'
import {
  CatalogoComponent
} from './Catalogo/catalogo.component'
import { PuestosComponent } from './puestos/puestos.component';
import{AyudasComponent} from './ayudas/ayudas.component'
import {SubMenuMantenimientoComponent} from './sub-menu-mantenimiento/sub-menu-mantenimiento.component'
import { RegisterComponent } from '../Usuario/register/register.component';
const routes: Routes = [{
  path: '',
  data: {
    title: 'Mantenimiento'
  },
  children: [{
    path: 'departamentos',
    component: DepartamentosComponent,
    data: {
      title: ''
    }
  },{
    path: 'catalogo/:id_catalogo/:descripcion',
    component: CatalogoComponent,
    data: {
      title: ''
    }
  },
  {
    path: 'puestos',
    component: PuestosComponent,
    data: {
      title: ''
    }

  },
  {
    path: 'ayudas',
    component: AyudasComponent,
    data: {
      title: ''
    }

  },
  {
    path: 'menu',
    component: SubMenuMantenimientoComponent,
    data: {
      title: ''
    }

  },
  {
    path: 'registross',
    component: RegisterComponent,
    data: {
      title: ''
    }

  }
]
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MantenimientoRoutingModule {}
