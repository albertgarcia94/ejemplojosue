import {
  Injectable
} from '@angular/core';
import {
  Http,
  Headers,
  RequestOptions,
  RequestMethod
} from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/Rx';

import {
  Configuration
} from '../../../ConfigSystems/constants';
import {
  Router
} from '@angular/router';


@Injectable()
export class ServiceDeptos {

  public lenguajeTable;
  public lengthMenu;
  public configDataTable;

  constructor(private http: Http,
    private _url: Configuration,
    private router: Router) {

  }

  private apiUrl = this._url.serverMantenimientos;




  public Get(url) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    //headers.append('Authorization', 'Bearer ' + sessionStorage.getItem('token'));
    let options = new RequestOptions({
      method: RequestMethod.Get,
      headers: headers
    });

    return this.http.get(this.apiUrl + url, options)
      .map((res) => res.json());
  }





  public Send(data, url, type) {
    let typeService;
    if (type == 1) {
      typeService = RequestMethod.Post;
    } else {
      typeService = RequestMethod.Put;
    }

    let headers = new Headers();
    headers.append('Content-Type', 'application/json; charset=utf-8');
   // headers.append('Authorization', 'Bearer ' + sessionStorage.getItem('token'));
    let options = new RequestOptions({
      method: typeService,
      headers: headers
    });
    return this.http.post(this.apiUrl + url, data, options)
      .map(res => res.json());
  }

  public redirect() {
    this.router.navigate(['/401']);
  }


}
