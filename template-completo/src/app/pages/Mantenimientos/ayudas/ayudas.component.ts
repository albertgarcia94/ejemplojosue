import {
  BsModalService,
  BsModalRef
} from 'ngx-bootstrap/modal';

import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  TemplateRef
} from '@angular/core';
import {
  DataTableDirective
} from 'angular-datatables';
import {
  Subject
} from 'rxjs';
import {
  ToastrService
} from 'ngx-toastr';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';

import {
  Service
} from '../../../service/service.service'
import {
  ServiceDeptos
} from '../ServiceDeptos/ServiceDeptos'
import {
  ServiceConfig
} from '../../../ServiceGlobal/ConfigDataTable';
import {
  SwallAlertGlobal
} from '../../../ServiceGlobal/Alertas'
import {
  Validaciones
} from '../../../ServiceGlobal/Validaciones'
import {
  DataDefinitions
} from '../ServiceDeptos/DataDefinitions'

@Component({
  selector: 'app-ayudas',
  templateUrl: './ayudas.component.html',
  styleUrls: ['./ayudas.component.css'],
  providers: [ServiceConfig, SwallAlertGlobal, Validaciones, Service, ServiceDeptos, DataDefinitions]

})
export class AyudasComponent implements AfterViewInit, OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtTrigger = new Subject();
  modalRef: BsModalRef;

  titulo;

  constructor(
    private modalService: BsModalService,
    private configDatatable: ServiceConfig,
    private toastr: ToastrService,
    private swall: SwallAlertGlobal,
    private validar: Validaciones,
    private service: Service,
    private serviceDeptos: ServiceDeptos,
    private serviceDefinitions: DataDefinitions
  ) {
    this.validar.validationCharacter();
    this.validar.replaceSpace();

    this.dtOptions = this.configDatatable.ConfigDataTable();

    // se llama en el constructor para que solo cargue una vez
    this.loadEstados()

    //tipo de ayuda 
    this.loadTipoAyudas()


  }


  ngOnInit() {

    this.titulo="Agregar nuevo ayuda"

    this.serviceDefinitions.form = new FormGroup({
      'nombre': new FormControl(null, [
        Validators.required,
      ]),
      'descripcion': new FormControl(null, [
        Validators.required,
      ]),
      'idDetalleCatalogo': new FormControl(null, [
        Validators.required
      ]),
      'estadoRegistro': new FormControl(null, [
        Validators.required
      ]),
    })

    this.serviceDefinitions.ayuda = {
      idServiciosAyuda: "",
      nombre: "",
      descripcion: "",
      idDetalleCatalogo: "",
      estadoRegistro:""
    }

    this.loadAyudas()

  }





  loadAyudas() {

    this.serviceDeptos.Get(this.serviceDefinitions.uriAyudas)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.serviceDefinitions.dataAyuda = data.data;
          } else if (data.status == 400) {
            this.toastr.warning(data.message, 'Advertencia!');

          } else {
            this.toastr.error(data.message, 'Error!');

          }
          this.rerender();
        },
        Error => {
          if (Error.status == 401) {
            this.service.redirect();
          } else {
            this.toastr.error('Ocurrio un error con el servicio', 'Error!');
          }
        }
      );

  }


  loadEstados() {


    // se guarda en sesion storage para que solo cargue una vez en todo el sistema, esto
    //se agrega en todos los componentes
    if(sessionStorage.getItem('estados')){
        this.serviceDefinitions.estados=JSON.parse(sessionStorage.getItem('estados'))
        return
  
      }
      
    this.serviceDeptos.Get(this.serviceDefinitions.uriEstados)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.serviceDefinitions.dataAyuda = data.data;
            sessionStorage.setItem("estados",JSON.stringify(data.data))
          } else if (data.status == 400) {
            this.toastr.warning(data.message, 'Advertencia!');

          } else {
            this.toastr.error(data.message, 'Error!');

          }
          this.rerender();
        },
        Error => {
          if (Error.status == 401) {
            this.service.redirect();
          } else {
            this.toastr.error('Ocurrio un error con el servicio', 'Error!');
          }
        }
      );

  }

  loadTipoAyudas() {


    // se guarda en sesion storage para que solo cargue una vez en todo el sistema, esto
    //se agrega en todos los componentes
    if(sessionStorage.getItem('tipoAyuda')){
        this.serviceDefinitions.estados=JSON.parse(sessionStorage.getItem('estados'))
        //return
      }
      
    this.serviceDeptos.Get(this.serviceDefinitions.uriTipoAyuda)
      .subscribe(
        data => {
          if (data.status == 200) {
            this.serviceDefinitions.dataTipoAyuda = data.data;
            sessionStorage.setItem("tipoAyuda",JSON.stringify(data.data))
          } else if (data.status == 400) {
            this.toastr.warning(data.message, 'Advertencia!');

          } else {
            this.toastr.error(data.message, 'Error!');

          }
          this.rerender();
        },
        Error => {
          if (Error.status == 401) {
            this.service.redirect();
          } else {
            this.toastr.error('Ocurrio un error con el servicio', 'Error!');
          }
        }
      );

  }

  Save(item) {
    var body;
    if (this.serviceDefinitions.action == 1) {
      body = JSON.stringify({
     
        nombre: item.nombre,
        descripcion: item.descripcion,
        idDetalleCatalogo:item.idDetalleCatalogo,
        estadoRegistro:item.estadoRegistro
      })
    } else {
      body = JSON.stringify({
        idServiciosAyuda: item.idServiciosAyuda,
        nombre: item.nombre,
        descripcion: item.descripcion,
        idDetalleCatalogo:item.idDetalleCatalogo,
        estadoRegistro:item.estadoRegistro
      });
    }


     this.serviceDeptos.Send(body, this.serviceDefinitions.uriAyudas, this.serviceDefinitions.action)
        .subscribe(
          data => {
            if (data.status == 200) {
              this.swall.swallAlert(data.message, 1)
              this.hide()
            } else if (data.status == 402) {
              this.swall.swallAlert(data.message, 2)

            }else {
              this.swall.swallAlert(data.message, 3)
            }
          },
          Error => {
            if (Error.status == 401) {
              this.service.redirect();
            } else {
              this.swall.swallAlert("Ocurrio un error!", 3)
            }
          }
        );
  }

  edit(itemplate: TemplateRef < any > , item) {
    if (item != 0 && item != null && item != "") {
      this.serviceDefinitions.ayuda = item;
      this.titulo = "Editar ayuda ";
      this.serviceDefinitions.action = 2;

    } else {
      this.titulo = "Nuevo ayuda";
      this.serviceDefinitions.action = 1;
    }
    this.modalRef = this.modalService.show(itemplate,this.configDatatable.config);

  }

  show(template: TemplateRef < any > ): void {
    this.modalRef = this.modalService.show(template,this.configDatatable.config);
  }

  hide(): void {
    this.modalRef.hide()
    this.serviceDefinitions.action = 1;
    this.ngOnInit()
  }
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }


  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }



  Back() {
    window.history.back();
  }

  Next() {
    window.history.forward();
  }







}
