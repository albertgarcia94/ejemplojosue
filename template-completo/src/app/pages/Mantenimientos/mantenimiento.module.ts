import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import {MantenimientoRoutingModule} from './mantenimiento-routing.module'
import {DepartamentosComponent} from './Departamentos/departamentos.components'
import {CatalogoComponent} from './Catalogo/catalogo.component';
import { PuestosComponent } from './puestos/puestos.component';
import { AyudasComponent } from './ayudas/ayudas.component';
import { RegisterComponent } from './../Seguridad/register/register.component';
import { SubMenuMantenimientoComponent } from './sub-menu-mantenimiento/sub-menu-mantenimiento.component'
@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MantenimientoRoutingModule,
    ModalModule.forRoot(),
    CommonModule,
    DataTablesModule
  ],
  declarations: [
    DepartamentosComponent,
    CatalogoComponent,
    PuestosComponent,
    AyudasComponent,
    SubMenuMantenimientoComponent,
    // RegisterComponent

  ],

  exports: [
    ModalModule
  ]
})
export class MantenimientoModule { }
