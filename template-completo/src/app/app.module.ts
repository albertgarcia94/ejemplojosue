import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy, CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
//import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AppComponent } from './/app.component';
import { AppRoutingModule } from './/app-routing.module';
import { LayoutModule } from './/layouts/layout.module';
import { ScriptLoaderService } from './_services/script-loader.service';
import {ServiceConfig} from './ServiceGlobal/ConfigDataTable'
import {Configuration} from './ConfigSystems/constants';
import { PerfilesComponent } from './pages/Usuario/perfiles/perfiles.component';
import { PuestosUsuariosComponent } from './pages/Usuario/puestos-usuarios/puestos-usuarios.component';
import { TareasAsignarComponent } from './pages/Usuario/tareas-asignar/tareas-asignar.component';
import { PerfilComponent } from './pages/Usuario/perfil/perfil.component';
import { TareasRealizarComponent } from './pages/Usuario/tareas-realizar/tareas-realizar.component';
import { SeguridadModule } from './pages/Seguridad/seguridad.module';


@NgModule({
  declarations: [
    AppComponent,
    
    
    
    
        
  ],
  imports: [
    
    CommonModule,
    
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    SeguridadModule,
    DataTablesModule,
    BrowserAnimationsModule,
   // ModalModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 1500,
      positionClass: 'toast-top-right',
      preventDuplicates: true
    }),
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule
  ],
  providers: [ScriptLoaderService,{
    provide: LocationStrategy,
    useClass: HashLocationStrategy,
  },
  ServiceConfig,
  Configuration
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
 