import swal from 'sweetalert2';
import {
  Injectable
} from '@angular/core';
declare var $;

@Injectable( 
  


)

export class SwallAlertGlobal {

  public urlGlobal;


  public swallAlert(message, tipo) {
    let type, title,timer;

    switch (tipo) {
      case 1:
        type = "success";
        title = "Exito!";
        timer=2000;
        break;
      case 2:
        type = "warning"
        title = "Advertencia"
        timer=300000;
        break;
      case 3:
        type = "error"
        title = "Error!"
        timer=300000;
        break;
    }

    swal({
      title: title,
      text: message,
      type: type,
      timer: timer
    });
  }





}
