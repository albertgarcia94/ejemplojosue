import { Injectable } from '@angular/core';
declare var $;
@Injectable()
export class Validaciones {
    

    public ConvetCharDataTable(){
      $.fn.DataTable.ext.type.search.string = function ( data ) {
        return ! data ?
            '' :
            typeof data === 'string' ?
                data
                    .replace( /έ/g, 'ε')
                    .replace( /ύ/g, 'υ')
                    .replace( /ό/g, 'ο')
                    .replace( /ώ/g, 'ω')
                    .replace( /ά/g, 'α')
                    .replace( /ί/g, 'ι')
                    .replace( /ή/g, 'η')
                    .replace( /\n/g, ' ' )
                    .replace( /[áÁ]/g, 'a' )
                    .replace( /[éÉ]/g, 'e' )
                    .replace( /[íÍ]/g, 'i' )
                    .replace( /[óÓ]/g, 'o' )
                    .replace( /[úÚ]/g, 'u' )
                    .replace( /ê/g, 'e' )
                    .replace( /î/g, 'i' )
                    .replace( /ô/g, 'o' )
                    .replace( /è/g, 'e' )
                    .replace( /ï/g, 'i' )
                    .replace( /ü/g, 'u' )
                    .replace( /ã/g, 'a' )
                    .replace( /õ/g, 'o' )
                    .replace( /ç/g, 'c' )
                    .replace( /ì/g, 'i' ) :
                data;
    };
    
  
          var table = $('.datatable-table');
     
          // Remove accented character from search input as well
          $('#datatable-table_filter input').keyup( function () {
            table
              .search( 
                $.fn.DataTable.ext.type.search.string( $(this).value )
              )
              .draw();
          } );
   
    }

    public validationCharacter() {
        $('body').on('keypress', '.sololetras', function (e) {
          var key = e.keyCode || e.which;
          var tecla = String.fromCharCode(key).toLowerCase();
          var letras = " abcdefghijklmnñopqrstuvwxyzéáíóú,.";
          var especiales: any = "8-37-39-46";
    
          var tecla_especial = false;
          for (var i in especiales) {
            if (key == especiales[i]) {
              tecla_especial = true;
              break;
            }
          }
    
          if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
          }
          var text = $(this).val();
          text = text.replace(/\s+/gi, ' ');
          $(this).val(text);
    
        });
      }
    
      public validationCharacterSignos() {
        $('body').on('keypress', '.sololetrasignos', function (e) {
          var key = e.keyCode || e.which;
          var tecla = String.fromCharCode(key).toLowerCase();
          var letras = " abcdefghijklmnñopqrstuvwxyzéáíóú,./()¡!?¿-";
          var especiales: any = "8-37-39-46";
    
          var tecla_especial = false;
          for (var i in especiales) {
            if (key == especiales[i]) {
              tecla_especial = true;
              break;
            }
          }
    
          if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
          }
          var text = $(this).val();
          text = text.replace(/\s+/gi, ' ');
          $(this).val(text);
    
        });
      }
     
      public replaceSpace() {
        $('body').on('keypress', '.sinespacio', function (e) {
    
          let key = e.keyCode ? e.keyCode : e.which;
          if (key == 32) {
            return false;
          }
          var text = $(this).val();
          text = text.replace(/\s+/gi, '');
          $(this).val(text);
    
        });
      } 
    
      public validationCharacterNumerick() {
        $('body').on('keypress', '.letrasandnum', function (e) {
          var key = e.keyCode || e.which;
          var tecla = String.fromCharCode(key).toLowerCase();
          var letras = " abcdefghijklmnñopqrstuvwxyz1234567890";
          var especiales: any = "8-37-39-46";
    
          var tecla_especial = false;
          for (var i in especiales) {
            if (key == especiales[i]) {
              tecla_especial = true;
              break;
            }
          }
    
          if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
          }
          var text = $(this).val();
          text = text.replace(/\s+/gi, ' ');
          $(this).val(text);
        });
      }

      public validationNumerosGuion() {
        $('body').on('keypress', '.letrasguion', function (e) {
          var key = e.keyCode || e.which;
          var tecla = String.fromCharCode(key).toLowerCase();
          var letras = " 1234567890-+";
          var especiales: any = "8-37-39-46";
    
          var tecla_especial = false;
          for (var i in especiales) {
            if (key == especiales[i]) {
              tecla_especial = true;
              break;
            }
          }
    
          if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
          }
          var text = $(this).val();
          text = text.replace(/\s+/gi, ' ');
          $(this).val(text);
        });
      }

      public validationNumberPoint() {
        $('body').on('keypress', '.numeros_punto', function (e) {
          var key = e.keyCode || e.which;
          var tecla = String.fromCharCode(key).toLowerCase();
          var letras = "1234567890+ ";
          var especiales: any = "8-37-39-46";
    
          var tecla_especial = false;
          for (var i in especiales) {
            if (key == especiales[i]) {
              tecla_especial = true;
              break;
            }
          }
    
          if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
          }
          var text = $(this).val();
          text = text.replace(/\s+/gi, ' ');
          $(this).val(text);
        });
      }
	  

      public validationNumeros() {
        $('body').on('keypress', '.numeros', function (e) {
          var key = e.keyCode || e.which;
          var tecla = String.fromCharCode(key).toLowerCase();
          var letras = " 1234567890%.";
          var especiales: any = "8-37-39-46";
    
          var tecla_especial = false;
          for (var i in especiales) {
            if (key == especiales[i]) {
              tecla_especial = true;
              break;
            }
          }
          if (letras.indexOf(tecla) == -1 && !tecla_especial ) {
            return false;
          }
          var text = $(this).val();
          text = text.replace(/\s+/gi, ' ');
          $(this).val(text);
        });
      }
    
      public validarRtn() {
        $('body').on('keypress', '.numeros_rtn', function (e) {
          var key = e.keyCode || e.which;
          var tecla = String.fromCharCode(key).toLowerCase();
          var letras = " 1234567890";
          var especiales: any = "8-37-39-46";
    
          var tecla_especial = false;
          for (var i in especiales) {
            if (key == especiales[i]) {
              tecla_especial = true;
              break;
            }
          }
          if (letras.indexOf(tecla) == -1 && !tecla_especial ||  $(this).val().length==14) {
            return false;
          }
          var text = $(this).val();
          text = text.replace(/\s+/gi, ' ');
          $(this).val(text);
        });
      }
      public soloNumeros() {
        $('body').on('keypress','.solonumero-punto',function(e) {
            var key = window.event ? e.which : e.keyCode ;
            return ((key >= 48 && key <= 57) || (key==8) || key==46 || key==45) ;
         });
      }
  
      public espacioInput() {
        $('body').on('keyup', '.espacioInput', function () {
          var text = $(this).val();
          text = text.replace(/(\d{4})(?=\S)/g, '$1 '); 
          $(this).val(text);
        });
      } 
    


      public FechaHoraNombre(item){
    let fecha
    let fechaString:string
    let hora,minuto,segundo,momentoActual;
    momentoActual = new Date()
    hora = momentoActual.getHours()
    minuto = momentoActual.getMinutes()
    segundo = momentoActual.getSeconds()
    try {
      fecha = item.split('-'); 
      fecha= fecha[2] + "-" +this.mesesfilter(fecha[1]) +"-" +fecha[0]+"T"+hora+":"+minuto+":"+segundo;
      
          
    } catch (error) {
      fecha= item
      
    }

   
    fechaString=item
    if(fechaString && fechaString.length>=10){
      return fecha
    }else{
      return item
    } 
   
      } 




      mesesfilter(date){

        let meses=[{
            num:"01",
            mes:"Enero",
            mesING:"January",
        },
            {
              num:"02",
              mes:"Febrero",
              mesING:"February"
          },{
            num:"03",
            mes:"Marzo",
            mesING:"March"
        },
        {
          num:"04",
          mes:"Abril",
          mesING:"April"
        },
        {
          num:"05",
          mes:"Mayo",
          mesING:"May"
        },
        {
          num:"06",
          mes:"Junio",
          mesING:"June"
        },
        {
          num:"07",
          mes:"Julio",
          mesING:"July"
        },
        {
          num:"08",
          mes:"Agosto",
          mesING:"August"
        },{
          num:"09",
          mes:"Septiembre",
          mesING:"September"
        },{
          num:"10",
          mes:"Octubre",
          mesING:"October"
        },{
          num:"11",
          mes:"Noviembre",
          mesING:"November"
        },{
          num:"12",
          mes:"Diciembre",
          mesING:"December"
        }]
    
    
        for(let x of meses){
          if(x.mes==date || x.mesING==date){
            return x.num
          }
    
        }
  
      }

       public FechaHora(item){
    let fechaString:string;     
    let fecha;
    let hora,minuto,segundo,momentoActual;
    momentoActual = new Date()
    hora = momentoActual.getHours()
    minuto = momentoActual.getMinutes()
    segundo = momentoActual.getSeconds()

   
    try {
      fecha = item.split('-'); 
      fecha=  item+"T"+hora+":"+minuto+":"+segundo;
      
          
    } catch (error) {
      fecha= item
      
    }
    fechaString=item
    if(fechaString && fechaString.length>=10){
      return fecha
    }else{
      return item
    }
     
      } 

}
