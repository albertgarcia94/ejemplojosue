import { Injectable } from '@angular/core';
/*import { Http, Headers, RequestOptions, RequestMethod } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';*/

import { Configuration } from '../ConfigSystems/constants';
import { Router } from '@angular/router';


@Injectable()
export class ServiceConfig {

  public lenguajeTable;
  public lengthMenu;
  public configDataTable;

  //private http: Http,
  constructor(
  private _url: Configuration,
  private router: Router) {

  }

  private apiUrl = this._url.ServerLocal_Security;

  
       
 

  public ConfigDataTable() {
    this.lenguajeTable = {
      "search": "Buscar:",
      "paginate": {
          "first": "Primero",
          "last": "Ultimo",
          "next": "Siguiente",
          "previous": "Anterior"
      },
      "emptyTable": "Datos no encontrados",
      "info": "Mostrando _START_ a  _END_ de _TOTAL_ Datos",
      "lengthMenu": "Cargados _MENU_ Datos",
      "zeroRecords": "No se encontrarón resultados",
      "infoEmpty": "Mostrando 0  de _MAX_ Datos",
      "infoFiltered": "(filtrado entre _MAX_ total de Datos)"
  
    };
  
    this.lengthMenu = [
      [10, 25, 50, -1],
      [10, 25, 50, "Todos"]
    ];
  
    this.configDataTable = {
      pagingType: 'full_numbers',
      language: this.lenguajeTable,
      lengthMenu: this.lengthMenu,
      dom: 'Bfrtip',
      stateSave: false,
      buttons: [
        {
          extend: 'colvis',
          text: 'Seleccionar Columnas',
        },
        {
          extend: 'columnVisibility',
          text: 'Mostrar Todo',
          visibility: true
      }
      ]
    }
  
    return this.configDataTable;
  }


  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  configLg={
    backdrop: true,
    ignoreBackdropClick: true,
    class:'modal-lg'
  }


  
 
}
